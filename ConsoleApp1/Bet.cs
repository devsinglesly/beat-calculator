using System.Collections.Generic;

namespace ConsoleApp1
{
    public class Bet
    {
        public string sport_key { get; set; }
        public string sport_nice { get; set; }
        public string[] teams { get; set; }
        public string commence_time { get; set; }
        public string home_team { get; set; }
        public List<Site> sites { get; set;  }
    }
}