using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace ConsoleApp1
{
    [JsonObject]
    public class ApiResponse
    {
        [JsonProperty]
        public bool success { get; set; }
        [JsonProperty]
        public List<Bet> data { get; set; }
    }
}