﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            testRemoteService();
            
            var result = calcThree(1000, 3, 4, 5);
            
            Console.WriteLine($"Coefficient 1: {result[0]} ({result[1]}), Coefficient No-Win: {result[2]} ({result[3]}), Coefficient 2: {result[4]} ({result[5]}),  Win 1: {result[6]}, No-Win: {result[7]}, Win 2: {result[8]}, Bet: ({result[9]})");
            
        }

        static void app(string[] args)
        {
            try
            {
                var result = calc(Convert.ToDouble(args[0]), Convert.ToDouble(args[1]), Convert.ToDouble(args[2]));
                Console.WriteLine($"Coefficient 1: {result[0]} ({result[1]}), Coefficient 2: {result[2]} ({result[3]}), Win 1: {result[4]}, Win 2: {result[5]}, Beat: ({result[6]})");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        static void testRemoteService()
        {
            string json = new WebClient().DownloadString(
                "https://api.the-odds-api.com/v3/odds?sport=soccer_epl&region=uk&mkt=h2h&apiKey=a4be2e5f035d0ebd8b48420b37bc4355");

            var response = JsonConvert.DeserializeObject<ApiResponse>(json);

            foreach (var team in response.data)
            {
                foreach (var site in team.sites)
                {
                    try
                    {
                        var result = calcThree(1000, Convert.ToDouble(site.odds.h2h[0]), Convert.ToDouble(site.odds.h2h[1]), Convert.ToDouble(site.odds.h2h[2]));
                        Console.WriteLine($"Coefficient 1: {result[0]} ({result[1]}), Coefficient No-Win: {result[2]} ({result[3]}), Coefficient 2: {result[4]} ({result[5]}),  Win 1: {result[6]}, No-Win: {result[7]}, Win 2: {result[8]}, Bet: ({result[9]})");
                        using (StreamWriter file = File.AppendText(Directory.GetCurrentDirectory() + "/winners-win-no-win"))
                        {
                            file.WriteLine(
$@"
Coefficient 1: {result[0]} ({result[1]}),
Coefficient No-Win: {result[2]} ({result[3]}),
Coefficient 2: {result[4]} ({result[5]}),
Win 1: {result[6]},
No-Win: {result[7]},
Win 2: {result[8]},
Bet: ({result[9]}),
Site: {site.site_nice},
Match: {team.teams[0]} vs {team.teams[1]}");
                        }

                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                    }
                }
            }
        }

        static void calcWins(double money, double xMax, double yMax)
        {

            double x1 = 1.01, x2 = xMax;
            double y1  = 1.01, y2 = yMax;

            double prev = 1.01;
     
            
            while (y1 <= y2)
            { 
                try
                {
                    var result = calc(money, x1, y1);
                    Console.WriteLine($"Win coef t1 = {x1}, t2 = {y1}. Money win = {result[4]}");
                    using (StreamWriter file = File.AppendText(Directory.GetCurrentDirectory() + "/winners"))
                    {
                        file.WriteLine(@"Coefficient 1: {result[0]} ({result[1]}), Coefficient 2: {result[2]} ({result[3]}), Win 1: {result[4]}, Win 2: {result[5]}, Beat: ({result[6]})");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                if (x1 < x2)
                {
                    x1 += .01;
                    continue;
                }

                if (x1 >= x2)
                {
                    x1 = 1.01;
                    y1 = prev += .01;
                }

            }
        }


        static double[] calc(double money, double c1, double c2, int depth = 10000000)
        {
             //double money = 1500;
             //double c1 = 1.2;
             //double c2 = 1.9;
            double w1 = 0, w2 = 0;

            c1 = Math.Round(c1, 2);
            c2 = Math.Round(c2, 2);

            double? m1 = null, m2 = null;
            int tries = 0;

            var result = new double[7];
            
            while (tries <= depth)
            {
                tries++;
                if (m1 == null && m2 == null)
                {
                    m1 = money / 2;
                    m2 = money / 2;
                    
                }

                w1 = ((double)m1 * c1);
                w2 = ((double)m2 * c2);
                
                //Console.WriteLine("m1 = {0}, m2 = {1}", m1, m2);

                

               // Console.WriteLine("w1 = {0}, w2 = {1}", w1, w2);
               
               if (Convert.ToInt32(w1) == Convert.ToInt32(w2) && w1 >= money && w2 >= money)
               {
                   // сколько ставить на первую команду
                   result[0] = Math.Round((double)c1, 2); 
                   result[1] = Math.Round((double)m1, 2);
                   
                   // сколько ставить на вторую команду
                   result[2] = Math.Round((double)c2, 2);
                   result[3] = Math.Round((double)m2, 2);
                   
                   // выигрыш первой и второй команды
                   result[4] = Math.Round((double)w1, 2);
                   result[5] = Math.Round((double)w2, 2);

                   result[6] = Math.Round((double) money, 2);
                   break;
               }
               
                if (w1 >= money && w2 >= money)
                {
                    // сколько ставить на первую команду
                    result[0] = Math.Round((double)c1, 2); 
                    result[1] = Math.Round((double)m1, 2);
                   
                    // сколько ставить на вторую команду
                    result[2] = Math.Round((double)c2, 2);
                    result[3] = Math.Round((double)m2, 2);
                   
                    // выигрыш первой и второй команды
                    result[4] = Math.Round((double)w1, 2);
                    result[5] = Math.Round((double)w2, 2);

                    result[6] = Math.Round((double) money, 2);
                }

                if (w1 > w2)
                {
                    m1--;
                    m2++; 
                }
                else
                {
                    
                    m1++;
                    m2--;
                }
                
                continue;
            }

            if (result[1] != (double)0)
            {
                return result;
            }

            throw new Exception($"No win. Prediction money team1/team2 {w1}/{w2}");
        }

        static void caclThreeWins(double money, double xMax, double yMax, double zMax)
        {
            double xMin = 1.01, yMin = 1.01, zMin = 1.01;

            double xPrev = 1.01, yPrev = 1.01;
            
            while (zMin <= zMax)
            {
                xMin = Math.Round(xMin, 2);
                yMin = Math.Round(yMin, 2);
                zMin = Math.Round(zMin, 2);
                
                try
                {
                    var result = calcThree(money, xMin, yMin, zMin);
                    Console.WriteLine($"Coefficient 1: {result[0]} ({result[1]}), Coefficient No-Win: {result[2]} ({result[3]}), Coefficient 2: {result[4]} ({result[5]}),  Win 1: {result[6]}, No-Win: {result[7]}, Win 2: {result[8]}, Bet: ({result[9]})");
                    using (StreamWriter file = File.AppendText(Directory.GetCurrentDirectory() + "/winners-win-no-win"))
                    {
                        file.WriteLine($"Coefficient 1: {result[0]} ({result[1]}), Coefficient No-Win: {result[2]} ({result[3]}), Coefficient 2: {result[4]} ({result[5]}),  Win 1: {result[6]}, No-Win: {result[7]}, Win 2: {result[8]}, Bet: ({result[9]})");
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }


        }


        static double[] calcThree(double money, double c1, double c2, double c3, int depth = 1000000)
        {
            c1 = Math.Round(c1, 2);
            c2 = Math.Round(c2, 2);
            c3 = Math.Round(c3, 2);
            
            double w1 = 0, w2 = 0, w3 = 0;

            double? m1 = null, m2 = null, m3 = null;

            var result = new double[10];
            
            for (int i = 0; i < depth; i++)
            {
                if (m1 == null && m2 == null && m3 == null)
                {
                    m1 = money / 3;
                    m2 = money / 3;
                    m3 = money / 3;
                }

                w1 = (double) m1 * c1;
                w2 = (double) m2 * c2;
                w3 = (double) m3 * c3;

                if (w1 > money && w2 > money && w3 > money)
                {
                    // сколько ставить на первую команду
                    result[0] = Math.Round((double)c1, 2);
                    result[1] = Math.Round((double)m1, 2);
                   
                    // сколько ставить на ничью
                    result[2] = Math.Round((double)c2, 2);
                    result[3] = Math.Round((double)m2, 2);
                   
                    // сколько ставить на вторую
                    result[4] = Math.Round((double)c3, 2);
                    result[5] = Math.Round((double)m3, 2);
                    
                    // первая, ничья, вторая
                    result[6] = Math.Round((double)w1, 2);
                    result[7] = Math.Round((double)w2, 2);
                    result[8] = Math.Round((double)w3, 2);

                    result[9] = Math.Round((double) money, 2);
                }


                if (w1 < w2 && w1 < money)
                {
                    if (w2 > money)
                    {
                        m2--;
                        m1++;
                    }
                }

                if (w1 < w3 && w1 <= money)
                {
                    if (w3 > money)
                    {
                        m3--;
                        m1++;
                    }
                }

                if (w2 < w1 && w2 <= money)
                {
                    if (w2 > money)
                    {
                        m1--;
                        m2++;
                    }
                }
                
                if (w2 < w3 && w2 <= money)
                {
                    if (w3 > money)
                    {
                        m3--;
                        m2++;
                    }
                }

                if (w3 < w1 && w3 <= money)
                {
                    if (w1 > money)
                    {
                        m1--;
                        m3++;
                    }
                }

                if (w3 < w2 && w3 <= money)
                {
                    if (w2 > money)
                    {
                        m2--;
                        m3++;
                    }
                }

                if (w1 >= money && w2 >= money && w3 >= money)
                {
                    var multi = (w1 + w2 + w3);

                    w1 = multi / 3;
                    w2 = multi / 3;
                    w3 = multi / 3;
                    
                    // сколько ставить на первую команду
                    result[0] = Math.Round((double)c1, 2);
                    result[1] = Math.Round((double)m1, 2);
                   
                    // сколько ставить на ничью
                    result[2] = Math.Round((double)c2, 2);
                    result[3] = Math.Round((double)m2, 2);
                   
                    // сколько ставить на вторую
                    result[4] = Math.Round((double)c3, 2);
                    result[5] = Math.Round((double)m3, 2);
                    
                    // первая, ничья, вторая
                    result[6] = Math.Round((double)w1, 2);
                    result[7] = Math.Round((double)w2, 2);
                    result[8] = Math.Round((double)w3, 2);

                    result[9] = Math.Round((double) money, 2);
                    
                    break;
                }

            }
            if (result[1] != (double)0)
            {
                return result;
            }

            throw new Exception($"No win. Prediction money team1/no-win/team2 {w1}/{w2}/{w3}");
        }

    }
}