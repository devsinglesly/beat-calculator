namespace ConsoleApp1
{
    public class Site
    {
        public string site_key { get; set; }
        public string site_nice { get; set; }
        public string last_update { get; set; }
        public Odds odds { get; set; }
    }
}